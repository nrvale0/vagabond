class vagabond::params {
  case $::osfamily {
    'debian': {}
    default: {fail("OS family ${::osfamily} not supported!")}
  }
}
