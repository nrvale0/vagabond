class vagabond::profile::devnode::params {
  case $::osfamily {
    'debian': {
      $packages = {
        'git' => {},
        'emacs24-nox' => {},
        'vim' => {},
        'vim-puppet' => {},
        'lxc' => {},
        'git-cola' => {},
        'puppet-el' => {},
        'python-pip' => {},
        'd3' => { provider  => 'npm', },
      }
    }
    default: {fail("OS family ${::osfamily} not supported!")}
  }
}
