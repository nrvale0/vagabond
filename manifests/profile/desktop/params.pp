class vagabond::profile::desktop::params {
  case $::osfamily {
    'debian': {
      $packages = {
        'chromium-browser' => { ensure => latest, },
        'firefox' => { ensure => latest, },
        'compizconfig-settings-manager' => {},
        'unity-tweak-tool' => {},
        'indicator-multiload' => {},
        'wireshark' => {},
        'nautilus-open-terminal' => {},
        'browser-plugin-vlc' => { ensure => 'latest', },
        'vlc' => { ensure => 'latest', },
        'gnome-documents' => {},
      }
    }
  }
}
