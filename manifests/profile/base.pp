class vagabond::profile::base {
  require vagabond::profile
  require vagabond::profile::base::repos

  if !defined_with_params(Package['vim-nox'], { ensure => installed, }) {
    package { 'vim-nox': ensure => installed, }
  }

  alternatives { 'editor': 
    path => '/usr/bin/vim.nox',
    require => Package['vim-nox'],
  }

  package { 'links': ensure => installed, }
  package { 'zsh': ensure => installed, }
  package { 'notify-osd': ensure => installed, }
  package { 'openjdk-6-jdk': ensure => installed, }
  package { 'tshark': ensure => installed, }
}
