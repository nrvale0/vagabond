class vagabond::profile::devnode(
) inherits vagabond::profile::devnode::params {
  require vagabond::profile
  include nodejs

  $package_defaults = { ensure => present, }

  create_resources('package', $packages, $package_defaults)
}
