class vagabond::profile::desktop inherits vagabond::profile::desktop::params {
  require vagabond::profile::base
  include keepass2
  include dropbox

  validate_hash($packages)
  $package_defaults = { ensure => installed, }
  create_resources('package', $packages, $package_defaults)
}
